<div align="center">
    <h1>Text and Image Recognition Pipeline on AWS</h1>
</div>

---
### Tools

1. **Terraform** was used as an Infrastructure as a Code(IaC) tool to define, build, change and version AWS Resources.
2. **GitLab** was used to store and version source code for IaC and both the object and text recognition part of the requirement.
3. **GitLab CI/CD** was used to create a CI/CD pipeline for the IaC and the the object and text recognition services
4. **Trivy** was used as a container vulnerability scanning tool to find out vulnerabilities in the Images that we built and stored in the GitLab Container Registries and integrate a sense of security into the pipeline.
5. **Docker** was used to containerize our services and deploy them along with their dependencies without having to worry about the setup and environment of the host machine.
6. **Maven** was used to manage and package the dependencies of our services into a single artifact that could be used to containerize our service.
7. **AWS** was used as our choice of cloud provider to setup the underlying infrastructure for our services and their functionalities.
---
### Cloud Infrastructure Setup

To replicate the infrastructure setup that I have done, we need to make sure the following basic things are in place:

1. The `/project1/iac` directory is uploaded to GitLab along with the `.gitlab-ci.yml` file.
2. The `AWS_ACCESS_KEY`, `AWS_SECRET_ACCESS_KEY`, and `AWS_SESSION_TOKEN` pipeline variables are set in the repository under `Settings -> CI/CD -> Variables`

Once these two conditions are met, we need to run the pipeline from Build -> Pipeline
Once the Validate and Plan jobs pass, you will need to manually run the Apply job to apply the configuration to AWS.

**Note:** Make a note of the server password from line 4 of `/project1/iac/userdata.sh`

---
### Application Setup

To replicate the application setup that I have done, we need to make sure the following basic things are in place:

1. The Infrastructure setup is successfully done as mentioned above.
2. You have the Elastic IPs for the two EC2 instances.
3. The `/project1/obj-recog-a` and `/project1/text-recog-b` directories have been uploaded to two separate GitLab repositories along with their respective `Dockerfiles`, `pom.xml` dependency files and `.gitlab-ci.yml` files.
4. You have generated a personal access token for your GitLab account as shown in the documentation here: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

Once these basic requirements are in place we need to set the CI/CD environment variables in both the repositories:

- Go to `Deploy -> Container Registry` and add the registry URL as a CI/CD variable called **CI_REGISTRY** at `Settings -> CI/CD -> Variables` in your registry.
- Add another variable called **SERVER_IP** along with one of the servers IP address (add the other one to the other repository).
- Along with these, also add the `AWS_ACCESS_KEY`, `AWS_SECRET_ACCESS_KEY`, and `AWS_SESSION_TOKEN` variables as done in the IaC repository here again.
- Add your GitLab personal access token as **GITLAB_ACCESS_TOKEN** in your CI/CD variables.
- Add your GitLab userID as **CI_REGISTRY_USER** variable.
- Add the password I asked you to take note of during the IaC setup as **SSH_PASSWORD** variable.
- Add `ec2-user` as **SERVER_USER** variable.
- The last thing we need to do is add our ssh key from `AWS Academy -> AWS Details` as **SSH_PRIVATE_KEY** variable.

Once these variables are set, we need to run the pipeline from `Build -> Pipeline` for the *Object Recognition*(`/project1/obj-recog-a`) repository first and then the *Text Recognition*(`/project1/text-recog-b`) repository once the Object Recognition pipeline is complete.

---
### Architecture

![Architecture](architecture.png)

---
### Breaking Down The Architecture

#### The IaC Pipleine

The IaC Pipeline has 3 stages

1. **Validate** - To validate the syntax of the code which exits with a status code if there is a syntax error.
2. **Plan** - The most important part of pipeline. It refers to the tf_state file which is stored in GitLabs http backend to maintain the state of the cloud enviroment and track the changes that are made using our repository. It then lets us know whicch resources need to be created, deleted and updated after comparing the current state of the code with that in the tf_state file and outputs a tf_plan file with all this information and passes it to the downstream Apply job.
3. **Apply** - The Apply job is a manual job which which will create, delete or update resources accoring the the tf_plan file that was passes to it by the upstream job. The reason for it being manual is because the plan should be checked properly first as any mistake made while applying could cause serious issues in the cloud enviroment.
3. **Destroy** - Destroy is an optional manual job as a part of the 3rd stage and is independent of Apply. It is just created in case we need to take down our entire infrastructure in one go if ever required.

---
#### The AWS Enviroment

1. **VPC** - I have created a VPC for the project from scratch to sandbox the ennviroment and give me more freedom while using IaC rather than using a default VPC. The VPC I created is complete with 

    - *An Internet Gateway*
    - *A Route Table*
    - *A Public Subnet*
2. **EC2 Instances** - Two AWS EC2 instances of size t3.micro have been created in the public subnet along with

    - A *Security Group* that only allows SSH from anywhere and HTTP and HTTPS from within the subnet only.
    - Two *Elastic IP* addresses for the two Instances as AWS Academy kept reassigning them for every new session and changing the GitLab CI/CD pipeline variables everytime felt like a hassle while testing.
    - The instances were assigned the `LabInstanceProfile` instance profile so that they could communicate with SQS and Rekognition using the access key, secret key and session token.

The EC2 instances were created with a *userdata script* for first time setups that enabled password authentication, set a password for the default ec2-user, installed docker to run our containers, installed net-tools for troubleshooting purposes and allowed docker to run without sudo for the ec2-user.
3. **AWS Rekognition** - Used to identify Labels and Texts from Objects in the S3 bucket(https://njit-cs-643.s3.us-east-1.amazonaws.com/).
4. **Simple Queueing Service(SQS)** - A queue was created to push the indexes of the S3 Bucket Objects from the public bucket mentioned above that had cars in their images with a confidence higher than 90% from the Object Recognition service on Instance A. The Text Recognition service on Instance B would then pull these indexes out of the queue and run a text recognition on these indexes to check if any of them had text images in them and then print the indexes of those with both cars and texts out into a log file on the Instance B.

---
#### The Application Pipelines

The stages for both the applications are the same and have 4 stages

1. **Build Artifact** - In this stage, I have used *Maven* to package the dependencies and build a .jar file for the application. This .jar file is then passsed on to the downstream job.
2. **Build Image** - This job passes the artifact(.jar file) to the *Dockerfile* to build the image for our container. Once this image is packaged, I have stored it on GitLabs container registry where we can easily manage it.
3. **Image Scan** - In this stage, I have made use *Trivy*(https://trivy.dev/) which is an open source security scanning tool that can be used to scan containers for vulnerabilities in the operating system or installed packages. This helps me add a layer of security to the lifecycle of the application.
4. **Deploy** - In this stage we deploy the application to the specific server using *SSH* and pass in the AWS credentials as environment variables to the container at runtime from the CI/CD environment variables.
---